Benchmarks used for BigDFT over the past years, updated to run on latest version.

- small datasets :
B80 : 
- Boron 80 atoms - 120 orbitals, PBE functional
- up to 60 processes
- ~16GB of memory
- convergence in 26 iterations : ~7 core hours

H2O : 
- H2O 32 molecules,  128 Orbitals, PBE0 hybrid functional - CPU and GPU versions
- up to 64 processes
- ~10GB of memory
- 5 iterations : ~5 core hours (CPU only)


- large datasets
Porphy : 
- Porphyrine - 2048 orbitals, PBE functional
- Up to 1024 processes
- ~256GB of memory
- 3 iterations : ~50 core hours (CPU only)
- notes : Submission scripts for Curie provided
          

Ribbon-Graphene :
- scaling experiments for linear vs cubic versions of BigDFT
- Graphene (memory estimation given for cubic version) : 
    - 7_4 : 224 orbitals, 8GB of memory, up to 112 MPI processes, linear: 3 core hours, cubic: 3 core hours
    - 16_9 : 1152 orbitals, ~220GB of memory, up to 576 MPI processes, linear: 100 core hours, cubic: 56 core hours
    - 21_12 : 2016 orbitals, ~640GB of memory, up to 1008 MPI processes, linear: 192 core hours, cubic: 184 core hours
    - 29_17 : 3944 orbitals, ~3.6TB of memory, up to 1972 MPI processes, linear: 400 core hours, cubic : > 1000 core hours
    - 35_21 : 5880 orbitals, ~7TB of memory, up to 2940 MPI processes, linear: 600 core hours
- Ribbon :
    - 6 : 216 orbitals, ~9GB of memory, up to 108  MPI processes, linear: 22 core hours, cubic: 6 core hours
    - 32 : 1152 orbitals, ~275GB of memory, up to 576  MPI processes, linear: 72 core hours, cubic: 144 core hours
    - 54 : 1944 orbitals, ~750GB of memory, up to 972  MPI processes, linear: 136 core hours, cubic: 424 core hours
    - 110 : 3960 orbitals, ~3TB of memory, up to 1980 MPI processes, linear: 312 core hours, cubic: > 1500 core hours
    - 162 : 5832 orbitals, ~10TB of memory, up to 2916 MPI processes, linear: 416 core hours, cubic: > 5000 core hours
- note: Python notebook included for pre/post processing of computation

UO2 :
- scaling experiment for CPU/GPU versions of PBE/PBE0, with Piz Daint submission script
- uo2_1 : 164 orbitals, ~6GB of memory, up to 82 MPI processes, PBE0 CPU convergence in 32 SCF iterations: 4 core hours, PBE0 GPU: 5 core hours
- uo2_2 : 1432 orbitals, ~200GB of memory, up to 716 MPI processes, PBE0 CPU 10 SCF iterations: 332 core hours, PBE0 GPU: 77 core hours
- uo2_3 : 5400 orbitals, ~1.5TB of memory, up to 2700 MPI processes, PBE0 CPU 3 SCF iterations : 11128 core hours, PBE0 GPU: 2256 core hours
- uo2_4 : 12800 orbitals, ~6TB of memory, up to 6400 MPI processes, PBE0 CPU 1 SCF iteration : 40000 core hours, PBE0 GPU: 4400 core hours
- uo2_5 : 17150 orbitals, ~7,5TB of memory, up to 8575 MPI processes, PBE0 GPU 1 SCF iteration: 8000 core hours
