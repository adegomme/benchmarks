!*************************************************************************************************************************************
Outline:
     General Information
     Quick Start (for JURECA, JUQUEEN or CLAIX)
     Not-That-Quick Start
     Contact
     Good Practices


!*************************************************************************************************************************************

     General Information
     -------------------

This is a benchmark workload for DFT code FLEUR.
The benchmarks with run through all self-consistency steps are denoted as "fleur_bla_Bla_conv",
 others only have 1 k-point and run 1 self-consistency step.

The workload consists of the following test systems:

 !-----------------------------------------------------------------------------------------------------------------------------------!
 ! Name                   ! # iterations ! # k-points ! real/complex ! # atoms ! # val.electrons !   nvd   !  LOs  !  lmax  !  Kmax  ! 
 !-----------------------------------------------------------------------------------------------------------------------------------!
 ! fleur_tiny_NaCl        !       1      !      1     !      c       !    64   !       192       !   6217  !   -   !   10   !   3.5  !
 ! fleur_tiny_NaCl_conv   !      22      !      2     !      c       !    64   !       192       !   6217  !   -   !   10   !   3.5  !
 ! fleur_small_AuAg       !       1      !      1     !      c       !   108   !      1188       !  15468  !   -   !   10   !   4.2  !
 ! fleur_small_AuAg_conv  !      55      !      4     !      c       !   108   !      1188       !  15468  !   -   !   10   !   4.2  !
 !-----------------------------------------------------------------------------------------------------------------------------------!
 ! fleur_mid_CuAg         !       1      !      1     !      c       !   256   !      2816       !  23724  !   -   !   10   !   4.1  !
 ! fleur_mid_CuAg_conv    !      50      !      1     !      c       !   256   !      2816       !  23724  !   -   !   10   !   4.1  !
 ! fleur_mid_GaAs         !       1      !      1     !      c       !   512   !      7168       !  60391  !   +   !    8   !   3.6  !
 ! fleur_mid_GaAs_conv    !      40      !      1     !      c       !   512   !      7168       !  60391  !   +   !    8   !   3.6  !
 !-----------------------------------------------------------------------------------------------------------------------------------!
 ! fleur_big_TiO2         !       1      !      1     !      c       !   1078  !      8628       ! 101858  !   +   !    8   !   4.3  !
 ! fleur_big_TiO2_conv    !      45      !      1     !      c       !   1078  !      8628       ! 101858  !   +   !    8   !   4.3  !
 !-----------------------------------------------------------------------------------------------------------------------------------!

To run this workload you definitely need JUBE and FLEUR.
You also might need HDF5 and ELPA libraries.
The release MaXR3.0 is default for this workload. 
Other versions should also work, but the automatic time extraction from output files may fail.


!*************************************************************************************************************************************

     Quick Start (for JURECA, JUQUEEN or CLAIX)
     ------------------------------------------

1. Make sure you have FLEUR and JUBE installed.

2. Provide the path to and arguments of your executable in the file "./executable.xml"

3. Change directory.  
   $ cd fleur_bla_Bla ("bla_Bla" being the test case of interest)

4. Run the benchmark.
   $ jube run fleur.xml --tag jureca      ! on JURECA
   $ jube run fleur.xml --tag jureca_boo  ! on JURECA booster
   $ jube run fleur.xml --tag juqueen     ! on JUQUEEN
   $ jube run fleur.xml --tag claix       ! on CLAIX, usual nodes
   $ jube run fleur.xml --tag claix_knl   ! on CLAIX, knl nodes

5. You can check if the job is ready by
   $ jube continue FLEUR_bench

6. Run the analysis of output data:
   $ jube analyse FLEUR_bench

7. Get the table with the time measurenments:
   $ jube result FLEUR_bench -i last 



!**************************************************************************************

     Not-That-Quick Start
     -------------------- 

The directory "platforms" contains script prototypes for specific machines. 
Known machines are JURECA, JURECA booster, JUQUEEN (Forschungszentrum Juelich), 
CLAIX (RWTH cluster) and RWTH (old RWTH cluster).

The file "executable.xml" contains information about your executable.
You have to set the path to your executable (variable "executable").
If you are using executable with arguments, add them to the variable "args_exec".
If you are using CLAIX and you want to use ELPA library, provide the path to it in the variable "LD_elpa_path"
If you are using one of the known machines, use the provided tags (--tag jureca/claix/rwth).
If you are using another machine, you have to configure the prototypes , put them in the "platform" directory,
and add the corresponding tag.

The directories "fleur_bla_Bla" contain scripts and data relevant for the test system "bla_Bla"

If you are using one of the known machines, 
please check if you are happy with the modules and evironment in the batch job prototype (../platforms/MACHINE/batch_job.in).

Parameters of you job are set in the file "job_congig.xml".
      "timelimit_hm"   is the time limit in hours:minutes
      "nodes"          is the number of nodes   
      "taskspernode"   is the number of MPI processes per node
      "threadspertask" is the number of OpenMP threads per MPI process
      "memoryptask"    is the memory limit per MPI process (relevant for CLAIX)
      "tasks"          is the whole number of MPI processes, will be calculated
If you set several values to the same parameter, the benchmark will run multiple times to iterate over all possible parameter combinations.

How to start the batch jobs:
on JURECA
$ jube run fleur.xml --tag jureca
on JURECA booster
$ jube run fleur.xml --tag jureca_boo
on JUQUEEN
$ jube run fleur.xml --tag juqueen
on CLAIX
$ jube run fleur.xml --tag claix
on CLAIX, KNL nodes
$ jube run fleur.xml --tag claix_knl

Jube will create a directory FLEUR_bench.
Every benchmark get its id.
For example, data produced for the benchmark with id 1 cam be found in the directory fleur_BlaBla/FLEUR_bench/000001

You can check if the batch jobs are ready:
$ jube continue FLEUR_bench

If you started several benchmarks with the same test system, use its id:
$ jube continue FLEUR_bench -i id

How to analyse the data:
$ jube analyse FLEUR_bench [-i id]

How to get the timing table:
$ jube result FLEUR_bench [-i id] 



!**************************************************************************************
          
      Good Practices
      --------------
My experience on the CLAIX (Intel Broadwell, 24 cores)

 !-----------------------------------------------------------------------------------------------!
 ! Name                   ! # iterations ! Good configuration  !  Wall-clock !  Resources per    !
 !                        !              !  (#nodes/MPI/OMP)   !  exec. time !  job (core*hours) !
 !-----------------------------------------------------------------------------------------------!
 ! fleur_tiny_NaCl        !       1      !        1/4/6        !    40 sec   !            0.3    ! 
 ! fleur_tiny_NaCl_conv   !      25      !        2/4/6        !    16 min   !            7      !
 ! fleur_small_AuAg       !       1      !        2/4/6        !     3 min   !            2.5    !
 ! fleur_small_AuAg_conv  !      50      !        8/4/6        !   3 hours   !          120      !
 ! fleur_mid_CuAg         !       1      !        4/4/6        !    10 min   !           16      !
 ! fleur_mid_CuAg_conv    !      50      !        4/4/6        !   4 hours   !          390      !
 ! fleur_mid_GaAs         !       1      !       16/4/6        !    30 min   !          200      !
 ! fleur_mid_GaAs_conv    !      40      !       32/4/6        !  10 hours   !         7700      !
 ! fleur_big_TiO2         !       1      !       64/4/6        !   2 hours   !         3100      !
 ! fleur_big_TiO2_conv    !      45      !      256/4/6        !  24 hours   !       140000      !
 !-----------------------------------------------------------------------------------------------!
                                                                                   


!**************************************************************************************
          
         Contact
         -------


If you have some questions or suggestions, please do not hesitate to contact me:

       u.alekseeva@fz-juelich.de


!**************************************************************************************

U.Alekseeva  ////  Juli  2018
